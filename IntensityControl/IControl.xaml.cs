﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Axes;
using SpectrumPanoramaControl;

namespace IntensityControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class IControl : UserControl
    {

        public delegate void DoubleDoubleEventHandler(double startFreq, double endFreq);
        public event DoubleDoubleEventHandler RangeChangeEvent;

        public IControl()
        {
            InitializeComponent();
            ReadConfigSSR("SSR.json");
            InitStartParams();
            InitComponent();

            //GenerateData();

            //InitTDArray();
        }

        public void SetLanguage(string param)
        {
            if (param.ToLower().Contains("ru"))
            {
                var trans = (Translations)this.Resources["translate"];
                trans.MHz = "МГц";
                trans.sec = "с";
            }
            if (param.ToLower().Contains("en"))
            {
                var trans = (Translations)this.Resources["translate"];
                trans.MHz = "MHz";
                trans.sec = "s";
            }
            if (param.ToLower().Contains("az"))
            {
                var trans = (Translations)this.Resources["translate"];
                trans.MHz = "MHs";
                trans.sec = "s";
            }
        }


        public delegate void SimpleDoubleEventHandler(double freq);
        public event SimpleDoubleEventHandler CursorOnFreq;

        List<SSRParametrs> parametrsOfSSR = new List<SSRParametrs>();
        public List<SSRParametrs> ParametrsOfSSR
        {
            get { return parametrsOfSSR; }
        }

        private Color _CursorColor;
        public Color CursorColor
        {
            get { return _CursorColor; }
            set
            {
                _CursorColor = value;
                CursorX.LineStyle.Color = value;
            }
        }

        private Color _TitleColor;
        public Color TitleColor
        {
            get { return _TitleColor; }
            set
            {
                _TitleColor = value;
                xAxis.Title.Color = value;
                yAxis.Title.Color = value;
            }
        }

        private Color _AxisColor;
        public Color AxisColor
        {
            get { return _AxisColor; }
            set
            {
                _AxisColor = value;
                xAxis.AxisColor = value;
                yAxis.AxisColor = value;
                iChart.ViewXY.GraphBorderColor = value;
            }
        }

        private Color _LabelsColor;
        public Color LabelsColor
        {
            get { return _LabelsColor; }
            set
            {
                _LabelsColor = value;
                xAxis.LabelsColor = value;
                yAxis.LabelsColor = value;

                yAxis.MajorDivTickStyle.Color = value;
                yAxis.MinorDivTickStyle.Color = value;
                yAxis.MajorGrid.Color = value;

                xAxis.MajorDivTickStyle.Color = value;
                xAxis.MinorDivTickStyle.Color = value;

                for (int i = 0; i < xAxis.CustomTicks.Count(); i++)
                {
                    xAxis.CustomTicks[i].Color = value;
                }

                xAxis.InvalidateCustomTicks();
            }
        }

        private Color _BackGround;
        public Color BackGround
        {
            get { return _BackGround; }
            set
            {
                _BackGround = value;
                iChart.ChartBackground.Color = value;
                //chart.ChartBackground.GradientFill = GradientFill.Solid;
            }
        }



        private void InitStartParams()
        {
            _GlobalNumberOfBands = parametrsOfSSR.Count;
            _GlobalRangeXmin = parametrsOfSSR.First().FminMHz;
            _GlobalRangeXmax = parametrsOfSSR.Last().FmaxMHz;
            _GlobalRangeYmax = 60;
            _GlobalRangeYmin = 0;

            _DotsPerBand = 9831;
        }

        private void InitComponent()
        {
            //GenerateCustomTicks(ref xAxis);

            var mRect = iChart.ViewXY.GetMarginsRect();
            var d = (-1) * (mRect.Bottom + 4);
            LBox.Offset.Y = (int)d;
        }

        private void GenerateData()
        {
            int N = 3;
            int M = 3;
            IntensityPoint[,] ips = new IntensityPoint[N, M];

            ips[0, 0].Value = -120;
            ips[0, 1].Value = -70;
            ips[0, 2].Value = -10;

            ips[1, 0].Value = -120;
            ips[1, 1].Value = -100;
            ips[1, 2].Value = -80;

            ips[2, 0].Value = -120;
            ips[2, 1].Value = -110;
            ips[2, 2].Value = -100;

            IGS.PixelRendering = true;

            IGS.Data = ips;
        }

        IntensityPoint[,] IPS = new IntensityPoint[2000, 60];
        private void InitTDArray()
        {
            for (int i = 0; i < 2000; i++)
                for (int j = 0; j < 60; j++)
                    IPS[i, j].Value = -120;
        }

        #region Properties

        private double _GlobalRangeXmin;
        public double GlobalRangeXmin
        {
            get { return _GlobalRangeXmin; }
            set
            {
                _GlobalRangeXmin = value;
                _ReCalc();
            }
        }

        private double _GlobalRangeXmax = 0;
        private double GlobalRangeXmax
        {
            get { return _GlobalRangeXmax; }
            set { _GlobalRangeXmax = value; }
        }

        private double _GlobalRangeYmin = 0;
        private double GlobalRangeYmin
        {
            get { return _GlobalRangeYmin; }
            set
            {
                _GlobalRangeYmin = value;
                SetYRange(_GlobalRangeYmin, _GlobalRangeYmax);
            }
        }

        private double _GlobalRangeYmax = 60;
        private double GlobalRangeYmax
        {
            get { return _GlobalRangeYmax; }
            set
            {
                _GlobalRangeYmax = value;
                SetYRange(_GlobalRangeYmin, _GlobalRangeYmax);
            }
        }

        private int _GlobalNumberOfBands;
        public int GlobalNumberOfBands
        {
            get { return _GlobalNumberOfBands; }
            set
            {
                _GlobalNumberOfBands = value;
                _ReCalc();
            }
        }

        private double _GlobalBandWidthMHz;
        public double GlobalBandWidthMHz
        {
            get { return _GlobalBandWidthMHz; }
            set
            {
                _GlobalBandWidthMHz = value;
                _ReCalc();
            }
        }

        private int _DotsPerBand = 9831;
        public int DotsPerBand
        {
            get { return _DotsPerBand; }
            set { _DotsPerBand = value; }
        }

        public double MinVisibleX
        {
            get { return xAxis.Minimum; }
            set
            {
                if ((value >= _GlobalRangeXmin) && (value < _GlobalRangeXmax) && (value < xAxis.Maximum))
                    xAxis.Minimum = value;
            }
        }
        public double MaxVisibleX
        {
            get { return xAxis.Maximum; }
            set
            {
                if ((value > _GlobalRangeXmin) && (value <= _GlobalRangeXmax) && (value > xAxis.Minimum))
                    xAxis.Maximum = value;
            }
        }
        private double MinVisibleY
        {
            get { return yAxis.Minimum; }
            set
            {
                if ((value >= _GlobalRangeYmin) && (value < _GlobalRangeYmax) && (value < yAxis.Maximum))
                    yAxis.Minimum = value;
            }
        }
        private double MaxVisibleY
        {
            get { return yAxis.Maximum; }
            set
            {
                if ((value > _GlobalRangeYmin) && (value <= _GlobalRangeYmax) && (value > yAxis.Minimum))
                    yAxis.Maximum = value;
            }
        }

        public string xAxisLabel
        {
            get { return xAxis.Title.Text; }
            set
            {
                xAxis.Title.Text = value;
            }
        }
        public string yAxisLabel
        {
            get { return yAxis.Title.Text; }
            set
            {
                yAxis.Title.Text = value;
            }
        }

        public bool yAxisLabelVisible
        {
            get { return yAxis.Title.Visible; }
            set
            {
                if (yAxis.Title.Visible != value)
                {
                    yAxis.Title.Visible = value;
                    iChart.ViewXY.Margins = newMarginsY(iChart.ViewXY.Margins, value);
                }
            }
        }
        public bool xAxisLabelVisible
        {
            get { return xAxis.Title.Visible; }
            set
            {
                if (xAxis.Title.Visible != value)
                {
                    xAxis.Title.Visible = value;
                    iChart.ViewXY.Margins = newMarginsX(iChart.ViewXY.Margins, value);
                }
            }
        }

        private Thickness newMarginsX(Thickness Margins, bool value)
        {
            return new Thickness(Margins.Left, Margins.Top, Margins.Right, Margins.Bottom + ((value) ? 15 : -15));
        }

        private Thickness newMarginsX2(Thickness Margins, bool value)
        {
            return new Thickness(Margins.Left, Margins.Top, Margins.Right, Margins.Bottom + ((value) ? 25 : -25));
        }

        private Thickness newMarginsY(Thickness Margins, bool value)
        {
            return new Thickness(Margins.Left + ((value) ? 20 : -20), Margins.Top, Margins.Right, Margins.Bottom);
        }

        public bool xRangeLabelVisible
        {
            get { return xAxis.LabelsVisible; }
            set
            {
                if (xAxis.LabelsVisible != value)
                {
                    xAxis.LabelsVisible = value;
                    iChart.ViewXY.Margins = newMarginsX2(iChart.ViewXY.Margins, value);
                }
            }
        }


        public double cursorX
        {
            get { return CursorX.ValueAtXAxis; }
            set { CursorX.ValueAtXAxis = value; }
        }

        #endregion

        #region Public Metods
        public void ChangeXRange(double startFreq, double endFreq)
        {
            if (!(startFreq < _GlobalRangeXmin || endFreq > GlobalRangeXmax))
                xAxis.SetRange(startFreq, endFreq);
        }

        public void PlotData(double[] data)
        {
            IGS.RangeMinY = 0;
            IGS.RangeMaxY = 60;
            //IGS.RangeMaxX = 3025;
            //IGS.RangeMinX = 25;

            IGS.RangeMinX = xAxis.Minimum;
            IGS.RangeMaxX = xAxis.Maximum;


            for (int i = 0; i < 59; i++)
            {
                for (int j = 0; j < 2000; j++)
                {
                    IPS[j, i] = IPS[j, i + 1];
                }
            }

            for (int i = 0; i < 2000; i++)
            {
                IPS[i, 59].Value = data[i];
            }

            IGS.PixelRendering = true;

            IGS.Data = IPS;
            IGS.InvalidateData();
        }

        public void PlotData(double[,] data)
        {
            IGS.RangeMinY = 0;
            IGS.RangeMaxY = 60;
            //IGS.RangeMaxX = 3025;
            //IGS.RangeMinX = 25;

            IGS.RangeMinX = xAxis.Minimum;
            IGS.RangeMaxX = xAxis.Maximum;


            //for (int i = 0; i < 59; i++)
            //{
            //    for (int j = 0; j < 2000; j++)
            //    {
            //        IPS[j, i] = IPS[j, i + 1];
            //    }
            //}

            //for (int i = 0; i < 2000; i++)
            //{
            //    IPS[i, 59].Value = data[i];
            //}

            IGS.PixelRendering = true;

            IGS.Data = IPS;
            IGS.InvalidateData();
        }

        public void PlotData(double xStart, double xEnd, double[,] data)
        {
            IGS.RangeMinY = 0;
            IGS.RangeMaxY = 60;

            IGS.RangeMinX = xStart;
            IGS.RangeMaxX = xEnd;

            int rows = data.GetUpperBound(0) + 1;
            int columns = data.Length / rows;

            for (int i = 0; i < 2000; i++)
            {
                for (int j = 0; j < 60; j++)
                {
                    IPS[i, j].Value = data[i, j];
                }
            }

            //for (int i = 0; i < 59; i++)
            //{
            //    for (int j = 0; j < 2000; j++)
            //    {
            //        IPS[j, i] = IPS[j, i + 1];
            //    }
            //}

            //for (int i = 0; i < 2000; i++)
            //{
            //    IPS[i, 59].Value = data[i];
            //}

            IGS.PixelRendering = true;

            IGS.Data = IPS;
            IGS.InvalidateData();
        }

        public void ClearData()
        {
            InitTDArray();
            IGS.Data = IPS;
            IGS.InvalidateData();
        }

        #endregion

        #region Private Metods

        private void SetOriginScale()
        {
            xAxis.SetRange(_GlobalRangeXmin, _GlobalRangeXmax);
            yAxis.SetRange(_GlobalRangeYmin, _GlobalRangeYmax);
        }

        private void GenerateCustomTicks(ref AxisX axisX)
        {
            double[] intervals = new double[] { 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1 };

            double interval = axisX.Maximum - axisX.Minimum;
            double intervaldiv = interval / 6;

            var abs = intervals.Select(x => Math.Abs(x - intervaldiv)).ToArray();
            double minvalue = abs.Min();
            int indexmin = Array.IndexOf(abs, minvalue);

            int start = (int)(axisX.Minimum / intervals[indexmin]);

            List<double> dLTickValues = new List<double>();
            dLTickValues.Add(axisX.Minimum);

            var firststep = start * intervals[indexmin];
            while (firststep + intervals[indexmin] < axisX.Maximum)
            {
                firststep = firststep + intervals[indexmin];
                if (Math.Abs(axisX.Minimum - firststep) >= intervals[indexmin] * 0.95)
                    dLTickValues.Add(firststep);
            }
            if (Math.Abs(axisX.Maximum - dLTickValues[dLTickValues.Count() - 1]) >= intervals[indexmin])
                dLTickValues.Add(axisX.Maximum);
            else
                dLTickValues[dLTickValues.Count() - 1] = axisX.Maximum;

            //Set custom ticks for X axis,
            int[] aTickValues = new int[] { 25, 500, 1000, 1500, 2000, 2500, 3025 };

            var dTickValues = dLTickValues.ToArray();

            axisX.CustomTicks.Clear();
            for (int i = 0; i < dTickValues.Length; i++)
            {

                CustomAxisTick cTick = new CustomAxisTick(
                    axisX,
                    dTickValues[i],
                    dTickValues[i].ToString("G5"),
                    6,
                    true,
                    axisX.MajorDivTickStyle.Color,
                    CustomTickStyle.TickAndGrid);

                axisX.CustomTicks.Add(cTick);

            }

            double width = intervals[indexmin];
            double minorwidth = width / 5d;

            List<double> lMinors = new List<double>();

            double first = dTickValues[1];
            double almfirst = dTickValues[0];
            while (almfirst <= first - minorwidth)
            {
                lMinors.Add(first - minorwidth);
                first = first - minorwidth;
            }

            for (int i = 1; i < dTickValues.Count() - 1; i++)
            {
                lMinors.Add(dTickValues[i] + 1 * minorwidth);
                lMinors.Add(dTickValues[i] + 2 * minorwidth);
                lMinors.Add(dTickValues[i] + 3 * minorwidth);
                lMinors.Add(dTickValues[i] + 4 * minorwidth);
            }

            double last = dTickValues[dTickValues.Count() - 1];
            double almlast = dTickValues[dTickValues.Count() - 2];
            while (almlast + minorwidth <= last)
            {
                lMinors.Add(almlast + minorwidth);
                almlast = almlast + minorwidth;
            }

            var minors = lMinors.ToArray();
            Array.Sort(minors);

            for (int i = 0; i < minors.Length; i++)
            {
                CustomAxisTick cTick = new CustomAxisTick(
                    axisX,
                    minors[i],
                    "",
                    3,
                    true,
                    axisX.MinorDivTickStyle.Color,
                    CustomTickStyle.Tick);

                axisX.CustomTicks.Add(cTick);
            }
            //Allow showing the custom tick strings

            axisX.CustomTicksEnabled = true;

            axisX.InvalidateCustomTicks();
        }

        private void _ReCalc()
        {
            _GlobalNumberOfBands = parametrsOfSSR.Count;
            _GlobalRangeXmin = parametrsOfSSR.First().FminMHz;
            _GlobalRangeXmax = parametrsOfSSR.Last().FmaxMHz;
            SetXRange(_GlobalRangeXmin, _GlobalRangeXmax);
        }

        private void SetXRange(double start, double end)
        {

            xAxis.SetRange(start, end);
        }

        private void SetYRange(double start, double end)
        {
            yAxis.SetRange(start, end);
        }

        #endregion


        #region ChartHandlers
        Popup p = new Popup();

        private void InitPopup()
        {
            p.IsOpen = false;
            p.AllowsTransparency = true;
        }

        private TextBlock generatePopupChild(double xValue)
        {
            TextBlock tb = new TextBlock();
            tb.Text = xValue.ToString("F4");
            //tb.Background = new SolidColorBrush(Colors.Black);
            tb.Foreground = new SolidColorBrush(Colors.White);
            return tb;
        }

        private void iChart_MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                {
                    var pos = e.GetPosition(iChart);
                    var marginsRect = iChart.ViewXY.GetMarginsRect();
                    //var rect2 = iChart.ViewXY.Margins.Right;

                    if ((pos.X >= marginsRect.X) && (pos.X <= marginsRect.X + marginsRect.Width) && (pos.Y >= marginsRect.Y) && (pos.Y <= marginsRect.Y + marginsRect.Height))
                    {

                        xAxis.CoordToValue((int)pos.X, out var xValue, true);
                        yAxis.CoordToValue((int)pos.Y, out var yValue, true);

                        CursorX.ValueAtXAxis = xValue;

                        CursorOnFreq?.Invoke(xValue);


                        bool popup = false;
                        if (popup)
                        {
                            p.Child = generatePopupChild(xValue);
                            p.PlacementTarget = mainGrid;

                            p.PlacementRectangle = new Rect(pos.X, pos.Y, 20, 20);

                            p.HorizontalOffset = (iChart.ActualWidth - e.GetPosition(iChart).X < iChart.ViewXY.Margins.Right * 2) ? (iChart.ViewXY.Margins.Right * -2) + 5 : 1;
                            p.VerticalOffset = (iChart.ActualHeight - e.GetPosition(iChart).Y < iChart.ViewXY.Margins.Bottom * 2) ? (iChart.ViewXY.Margins.Bottom * -1) : 0;

                            p.IsOpen = true;
                        }
                        //string MHz = "МГц";
                        //Label.Text = String.Format("{0} " + MHz, xValue.ToString("F4"));
                    }
                }
            }
        }

        private void iChart_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.LeftButton == MouseButtonState.Pressed)
            {
                LBox.Visible = !LBox.Visible;
            }
        }


        private void xAxis_RangeChanged(object sender, RangeChangedEventArgs e)
        {
            if (e.NewMax - e.NewMin > _GlobalRangeXmax - _GlobalRangeXmin)
            {
                SetOriginScale();
                RangeChangeEvent?.Invoke(xAxis.Minimum, xAxis.Maximum);
                return;
            }

            if (e.NewMax - e.NewMin < 1)
            {
                double center = (xAxis.Maximum + xAxis.Minimum) / 2d;
                xAxis.SetRange(center - 100 / 100d / 2d, center + 100 / 100d / 2d);
            }

            if (e.NewMax > _GlobalRangeXmax)
            {
                double shift = e.NewMax - _GlobalRangeXmax;
                xAxis.SetRange(e.NewMin - shift, e.NewMax - shift);
            }

            if (e.NewMin < _GlobalRangeXmin)
            {
                double shift = _GlobalRangeXmin - e.NewMin;
                xAxis.SetRange(e.NewMin + shift, e.NewMax + shift);
            }
            RangeChangeEvent?.Invoke(xAxis.Minimum, xAxis.Maximum);
            //GenerateCustomTicks(ref xAxis);
        }

        private void yAxis_RangeChanged(object sender, RangeChangedEventArgs e)
        {
            if (e.NewMax - e.NewMin > _GlobalRangeYmax - _GlobalRangeYmin)
            {
                SetOriginScale();
                return;
            }

            if (e.NewMax > _GlobalRangeYmax)
            {
                double shift = e.NewMax - _GlobalRangeYmax;
                yAxis.SetRange(e.NewMin - shift, e.NewMax - shift);
            }

            if (e.NewMin < _GlobalRangeYmin)
            {
                double shift = _GlobalRangeYmin - e.NewMin;
                yAxis.SetRange(e.NewMin + shift, e.NewMax + shift);
            }
        }


        #endregion

        #region ReadCreate ConfigSSR
        public void ReadConfigSSR(string bandTableFilename)
        {
            try
            {
                if (parametrsOfSSR.Count > 0)
                {
                    parametrsOfSSR.Clear();
                }

                var options = new JsonSerializerOptions { WriteIndented = true };
                string jsonString = File.ReadAllText(bandTableFilename);
                parametrsOfSSR = JsonSerializer.Deserialize<List<SSRParametrs>>(jsonString, options);
                if (parametrsOfSSR.Count > 0)
                {
                    GlobalRangeXmin = parametrsOfSSR.First().FminMHz;
                    GlobalRangeXmax = parametrsOfSSR.Last().FmaxMHz;
                    GlobalNumberOfBands = (short)parametrsOfSSR.Count;
                }
            }
            catch (Exception e)
            {
                CreateConfigSSR(bandTableFilename);
            }
        }

        private void CreateConfigSSR(string bandTableFilename)
        {
            short Fmin = 30;
            short Bandpass;

            for (int i = 0; i < 79; i++)
            {
                if (i <= 1 || i == 5)
                {
                    Bandpass = 30;
                    parametrsOfSSR.Add(new SSRParametrs((short)i, Fmin, (short)(Fmin + Bandpass)));
                    Fmin += Bandpass;
                }
                else if (i == 2 || i == 38 || i == 53 || i == 78)
                {
                    Bandpass = 40;
                    parametrsOfSSR.Add(new SSRParametrs((short)i, Fmin, (short)(Fmin + Bandpass)));
                    Fmin += Bandpass;
                }
                else if (i <= 4 || (i >= 6 && i <= 11) || (i >= 13 && i <= 37) || (i >= 39 && i <= 52) || (i >= 54 && i <= 64) || (i >= 66 && i <= 77))
                {
                    Bandpass = 80;
                    parametrsOfSSR.Add(new SSRParametrs((short)i, Fmin, (short)(Fmin + Bandpass)));
                    Fmin += Bandpass;
                }
                else if (i == 12 || i == 65)
                {
                    Bandpass = 60;
                    parametrsOfSSR.Add(new SSRParametrs((short)i, Fmin, (short)(Fmin + Bandpass)));
                    Fmin += Bandpass;
                }
            }

            var options = new JsonSerializerOptions { WriteIndented = true };
            string json = JsonSerializer.Serialize(parametrsOfSSR, options);
            File.WriteAllText(bandTableFilename, json);

            if (parametrsOfSSR.Count > 0)
            {
                GlobalNumberOfBands = (short)parametrsOfSSR.Count;
            }
        }
        #endregion

        private void mainGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            p.IsOpen = false;
        }
    }

    class Translations : INotifyPropertyChanged
    {
        private string _MHz;
        private string _sec;

        public Translations()
        {
            this._MHz = "МГц";
            this._sec = "с";
        }

        public string MHz
        {
            get { return _MHz; }
            set
            {
                _MHz = value;
                OnPropertyChanged("MHz");
            }
        }
        public string sec
        {
            get { return _sec; }
            set
            {
                _sec = value;
                OnPropertyChanged("sec");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }

    class DifConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double tot = 0;
            foreach (double value in values)
                tot -= value;
            //return tot.ToString();
            return tot;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
