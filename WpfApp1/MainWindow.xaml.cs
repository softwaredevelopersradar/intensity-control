﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Random random = new Random();

        private void one_Click(object sender, RoutedEventArgs e)
        {
            double[] arr = new double[2000];
            for (int i = 0; i < 2000; i++)
                arr[i] = random.NextDouble() * (-120);

            iControl.PlotData(arr);
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            iControl.ClearData();
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            iControl.CursorColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            iControl.TitleColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void five_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            iControl.AxisColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void six_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            iControl.LabelsColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void seven_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            iControl.BackGround = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void eight_Click(object sender, RoutedEventArgs e)
        {
            iControl.SetLanguage("rus");
        }

        private void nine_Click(object sender, RoutedEventArgs e)
        {
            iControl.SetLanguage("eng");
        }
    }
}
